CREATE TABLE "tbl_servers_visited"
(
    "visit_id" SERIAL,
    "server_name" STRING(100),
    PRIMARY KEY ("visit_id")
);

CREATE TABLE "tbl_server_details"
(
    "server_name" STRING(100) NOT NULL,
    "ssl_grade_date" TIMESTAMPTZ,
    "previous_ssl_grade" STRING(10),
    "servers" JSONB NULL,
    PRIMARY KEY ("server_name")
);

/*
CREATE TABLE "servers"
(
    "server_name" STRING(100) NOT NULL,
    "servers" JSONB NULL,
    PRIMARY KEY ("server_name")
);
*/

-- create database testmp_db;
-- set database = testmp_db;
-- select * from tbl_servers_visited
-- 
-- COCKROACH
-- SERVER: cockroach start --insecure
-- CLIENTE: cockroach sql --insecure