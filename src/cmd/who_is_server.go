package cmd

import (
	"os/exec"
	"strings"
)

const (
	COUNTRY_KEY      = "Country:"
	ORGANIZATION_KEY = "Organization:"
	EOF_STR          = "\n"
)

func GetCountryAndOwner(ipAddress string) (country, owner string) {
	cmd := exec.Command("whois", ipAddress)
	out, err := cmd.CombinedOutput()
	if err != nil {
		panic("Failed executing command.")
	}
	responseText := string(out)
	country = findValue(responseText, COUNTRY_KEY)
	owner = findValue(responseText, ORGANIZATION_KEY)
	return
}

func findValue(haystack, needle string) (value string) {
	keyIndex := strings.Index(haystack, needle)
	if keyIndex == -1 {
		panic("Who-is command error:: Key not found:" + needle)
		return
	}
	eofIndex := strings.Index(haystack[keyIndex:], EOF_STR)
	if eofIndex == -1 {
		panic("Who-is command error:: Key not found:" + EOF_STR)
		return
	}
	value = strings.TrimSpace(haystack[keyIndex+len(needle) : keyIndex+eofIndex])
	return
}
