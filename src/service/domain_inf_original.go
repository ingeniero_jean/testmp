package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"jeandev.co/testmp/v1/src/model"
)

const (
	API_ANALIZE_URL = "https://api.ssllabs.com/api/v3/analyze?host=​"
)

func GetDomainInfServiceOriginal(name string) (responseObj model.Response, err error) {
	url := API_ANALIZE_URL + name
	response, err := http.Get(url)

	defer response.Body.Close()
	if err != nil {
		return
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}
	json.Unmarshal(body, &responseObj)
	return
}
