package service

import (
	"database/sql"
	"sync"

	"jeandev.co/testmp/v1/src/entity"
	"jeandev.co/testmp/v1/src/model"
)

var serversVisited model.ServerVisited
var (
	dbSingleConnec *sql.DB
	lock           = &sync.Mutex{}
)

func AddServerVisited(serverName string) {
	//serversVisited.Items = append(serversVisited.Items, serverName)
	entity.SaveServerVisited(serverName)
}

func GetServerVisited() model.ServerVisited {
	//return serversVisited
	return entity.GetAllServerVisited()
}
