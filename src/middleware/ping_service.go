package middleware

import (
	"fmt"
	"net/http"

	"github.com/valyala/fasthttp"
)

func TestService(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		name := fmt.Sprintf("%s", ctx.UserValue("name"))

		req, err := http.NewRequest(http.MethodOptions, "http://"+name, nil)
		if err != nil {
			fmt.Println(err)
			panic("Error making request")
		}
		client := &http.Client{}
		_, err = client.Do(req)
		if err != nil {
			fmt.Println(err)
			panic("Error on response")
		}

		if ctx.Response.StatusCode() != fasthttp.StatusOK {
			fmt.Println(err)
			panic("Error on response")
		}
		next(ctx)
	}
}
