package middleware

import (
	"encoding/json"
	"fmt"

	"github.com/valyala/fasthttp"
	"jeandev.co/testmp/v1/src/model"
)

func HandlerPanic(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
				domainResponse := model.Domain{
					ServersChanged: false,
					Logo:           "Logo sacado del html",
					Title:          "Sacado del html",
					IsDown:         true,
				}
				domainsData, err := json.Marshal(domainResponse)
				ctx.Response.Header.Add("Content-Type", "application/json")
				if err != nil {
					ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
					return
				}
				ctx.SetStatusCode(fasthttp.StatusOK)
				ctx.Write(domainsData)
				return
			}
		}()
		next(ctx)
	}

}
