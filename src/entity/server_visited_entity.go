package entity

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"time"

	"jeandev.co/testmp/v1/src/dbconn"
	"jeandev.co/testmp/v1/src/model"
)

const (
	INSERT_SERVER_VISITED    = "INSERT INTO " + dbconn.SERVER_VISITED_TBL + " (server_name) VALUES ($1);"
	INSERT_SERVER_DETAIL     = "INSERT INTO " + dbconn.SERVER_DETAILS_TBL + " (server_name,ssl_grade_date,previous_ssl_grade, servers) VALUES ($1,NOW(),$2,$3);"
	UPDATE_SERVER_DETAIL     = "UPDATE " + dbconn.SERVER_DETAILS_TBL + " SET ssl_grade_date=NOW(), previous_ssl_grade=$1, servers=$3 WHERE server_name LIKE $2;"
	SELECT_THE_SERVER_DETAIL = "SELECT server_name,ssl_grade_date,previous_ssl_grade, servers  FROM " + dbconn.SERVER_DETAILS_TBL +
		" WHERE server_name LIKE $1 ;"
	SELECT_ALL_SERVERS_VISITED = "select server_name FROM " + dbconn.SERVER_VISITED_TBL + ";"
	TIME_FORMAT                = "2006-01-02T15:04:05.000000Z"
)

func SaveServerVisited(name string) error {
	db, err := dbconn.GetDBConnection()
	if err != nil {
		return err
	}
	//sqlCommand := fmt.Sprintf(INSERT_SERVER_VISITED, name)
	if _, err = db.Query(INSERT_SERVER_VISITED, name); err != nil {
		return err
	}
	return nil
}

func GetAllServerVisited() (serverVisited model.ServerVisited) {
	db, err := dbconn.GetDBConnection()

	rows, err := db.Query(SELECT_ALL_SERVERS_VISITED)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var serverName string

		if err := rows.Scan(&serverName); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Servername : %d \n", serverName)
		serverVisited.Items = append(serverVisited.Items, serverName)
	}
	return
}

// SaveServerDetail update or update only if has upgrate more than an hour
func SaveServerDetail(serverName string, domain *model.Domain) error {
	db, err := dbconn.GetDBConnection()
	rows, err := db.Query(SELECT_THE_SERVER_DETAIL, serverName)
	//+ "AND grades.create_at >= (NOW() - INTERVAL '1 hours' )"
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer rows.Close()
	foundRows := false
	for rows.Next() {
		foundRows = true
		var serverName, sslGradeDate, previousSslGrade, servers string

		if err := rows.Scan(&serverName, &sslGradeDate, &previousSslGrade, &servers); err != nil {
			log.Fatal(err)
			return err
		}

		sslGradeDB, err := time.Parse(TIME_FORMAT, sslGradeDate)
		if err != nil {
			fmt.Println(err)
			return err
		}

		currentDate := time.Now()
		if currentDate.After(sslGradeDB.Add(1 * time.Hour)) {
			domain.PreviousSslGrade = domain.SslGrade
			//verifyServerChanges(domain, servers)
			serversDBByte, err := json.Marshal(servers)
			if err != nil {
				fmt.Println(err)
				return err
			}
			domainServerByte, err := json.Marshal(domain.Servers)
			if err != nil {
				fmt.Println(err)
				return err
			}

			if err != nil {
				fmt.Println(err)
				return err
			}
			if res := bytes.Compare(domainServerByte, serversDBByte); res != 0 {
				domain.ServersChanged = true
			}
			err = UpdateServerDetails(serverName, domain)
			if err != nil {
				fmt.Println(err)
				return err
			}
		} else {
			domain.PreviousSslGrade = previousSslGrade
		}

	}
	if !foundRows {
		err = InsertServerDetail(serverName, domain)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	return nil
}

func verifyServerChanges(domain *model.Domain, servers string) {
	serversDB := model.Server{}
	json.Unmarshal(([]byte(servers)), serversDB)
	var areEquals bool
	for _, serverDB := range servers {
		areEquals = false
		for _, serverDom := range domain.Servers {
			if reflect.DeepEqual(serverDom, serverDB) {
				areEquals = true
				break
			}
		}
		if areEquals == false {
			fmt.Println("Los servidores han cambiado")
			domain.ServersChanged = true
			return
		}
	}
	fmt.Println("Los servidores siguen igual")
}

func UpdateServerDetails(serverName string, domain *model.Domain) error {
	db, err := dbconn.GetDBConnection()
	if err != nil {
		return err
	}
	serverByte, err := json.Marshal(domain.Servers)
	if err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = db.Query(UPDATE_SERVER_DETAIL, domain.SslGrade, serverName, string(serverByte)); err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println("tbl_server_details actualizada.")
	fmt.Println(string(serverByte))
	domain.PreviousSslGrade = domain.SslGrade
	return nil
}

func InsertServerDetail(name string, domain *model.Domain) error {
	db, err := dbconn.GetDBConnection()
	if err != nil {
		return err
	}
	serverByte, err := json.Marshal(domain.Servers)
	if err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = db.Query(INSERT_SERVER_DETAIL, name, domain.SslGrade, string(serverByte)); err != nil {
		fmt.Println(err)
		return err
	}
	domain.PreviousSslGrade = domain.SslGrade
	return nil
}
