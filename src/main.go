package main

import (
	"log"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"jeandev.co/testmp/v1/src/handler"
	"jeandev.co/testmp/v1/src/middleware"
)

func main() {
	router := fasthttprouter.New()
	router.GET("/domain/:name", middleware.HandlerPanic(middleware.TestService(handler.DomainInfoGet())))
	router.GET("/server/visited", handler.GetServersVisited())
	//log.Fatal(fasthttp.ListenAndServe(":8081", router.Handler))
	if err := fasthttp.ListenAndServe(":8081", handler.CORS(router.Handler)); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}

}
