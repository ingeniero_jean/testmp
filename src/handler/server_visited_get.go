package handler

import (
	"encoding/json"

	"github.com/valyala/fasthttp"
	"jeandev.co/testmp/v1/src/service"
)

func GetServersVisited() fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		serverVisitedData, err := json.Marshal(service.GetServerVisited())
		if err != nil {
			ctx.Error("Marshalling error", fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.Add("Content-Type", "application/json")
		ctx.SetStatusCode(fasthttp.StatusAccepted)
		ctx.Write(serverVisitedData)
	}

}
