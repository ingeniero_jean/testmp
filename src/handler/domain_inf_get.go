package handler

import (
	"encoding/json"
	"fmt"

	"github.com/valyala/fasthttp"
	"jeandev.co/testmp/v1/src/cmd"
	"jeandev.co/testmp/v1/src/entity"
	"jeandev.co/testmp/v1/src/model"
	"jeandev.co/testmp/v1/src/service"
)

func DomainInfoGet() fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		name := fmt.Sprintf("%s", ctx.UserValue("name"))
		domains, err := service.GetDomainInfServiceOriginal(name)
		if err != nil {
			ctx.Error("Error getting domain", fasthttp.StatusInternalServerError)
			return
		}

		domainResponse := model.Domain{
			ServersChanged:   false,
			SslGrade:         "Campo no encontrado",
			PreviousSslGrade: "Campo no encontrado",
			Logo:             "Logo sacado del html",
			Title:            "Sacado del html",
			IsDown:           false,
		}
		for _, endpoint := range domains.Endpoints {
			country, owner := cmd.GetCountryAndOwner(endpoint.IPAddress)
			server := model.Server{
				Address:  endpoint.IPAddress,
				SslGrade: endpoint.Grade,
				Country:  country,
				Owner:    owner,
			}
			domainResponse.Servers = append(domainResponse.Servers, server)
			domainResponse.SslGrade = endpoint.Grade
		}
		entity.SaveServerDetail(name, &domainResponse)
		entity.SaveServerVisited(name)

		domainsData, err := json.Marshal(domainResponse)
		if err != nil {
			ctx.Error("Marshalling error", fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.Add("Content-Type", "application/json")
		ctx.SetStatusCode(fasthttp.StatusOK)
		ctx.Write(domainsData)
	}
}
