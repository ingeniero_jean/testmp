const app = new Vue({
    el: '#app',
    data() {
        return {
            titulo: 'Test MP - Jean',
            servers: [
                'server Manzana', 'server Pera', 'Bocadillo'
            ],
            domainRequest: '',
            dataDomains: null,
            serverVisited:null
            
        }
    },
    methods: {
        getDomain() {
            this.dataDomains = ''
            var url = 'http://localhost:8081/domain/'+this.domainRequest;
            axios
            .get(url)
            .then(response => (this.dataDomains = response.data))
            this.domainRequest = ''
        },
        getServerVisited(){
            var url = 'http://localhost:8081/server/visited';
            axios
            .get(url)
            .then(response => (this.serverVisited = response.data))
        }
    },
    computed: {
        isDisabled: function(){
          return !this.domainRequest.trim().length>0;
      }
    }
})
