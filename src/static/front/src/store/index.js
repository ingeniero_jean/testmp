import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  data() {
    return {
        titulo: 'Test MP - Jean',
        servers: [
            'server Manzana', 'server Pera', 'Bocadillo'
        ],
        domainRequest: '',
        dataDomains: null,
        serverVisited:null
        
    }
},
methods: {
},
computed: {
}
})
