package dbconn

import (
	"database/sql"
	"sync"

	_ "github.com/lib/pq"
)

var (
	dbSingleConnec *sql.DB
	lock           = &sync.Mutex{}
)

func createDBConnection() (db *sql.DB, err error) {
	db, err = sql.Open("postgres", "postgresql://root@localhost:26257/"+DB_NAME+"?sslmode=disable")

	//"postgresql://root@localhost:26257/"+
	// DB_NAME+"?sslmode=disable"
	//)
	return
}

func GetDBConnection() (sql.DB, error) {
	lock.Lock()
	defer lock.Unlock()
	var err error
	if dbSingleConnec == nil {
		dbSingleConnec, err = createDBConnection()
	}

	return *dbSingleConnec, err
}
