package model

type Response struct {
	Host            string     `json:"host"`
	Port            int64      `json:"port"`
	Protocol        string     `json:"protocol"`
	IsPublic        bool       `json:"isPublic"`
	Status          string     `json:"status"`
	StartTime       int64      `json:"startTime"`
	TestTime        int64      `json:"testTime"`
	EngineVersion   string     `json:"engineVersion"`
	CriteriaVersion string     `json:"criteriaVersion"`
	Endpoints       []Endpoint `json:"endpoints"`
}

type Endpoint struct {
	IPAddress         string  `json:"ipAddress"`
	StatusMessage     string  `json:"statusMessage"`
	Grade             string  `json:"grade"`
	GradeTrustIgnored string  `json:"gradeTrustIgnored"`
	HasWarnings       bool    `json:"hasWarnings"`
	IsExceptional     bool    `json:"isExceptional"`
	Progress          int64   `json:"progress"`
	Duration          int64   `json:"duration"`
	Delegation        int64   `json:"delegation"`
	ServerName        *string `json:"serverName,omitempty"`
}
