# TestMP

Prueba de Jean

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/ingeniero_jean/testmp/src/master/)

Front y back se encuentran en el mismo repo. Para correr el proyecto se procede así:


# Cockroach
```sh
  - SERVER: cockroach start --insecure
  - CLIENTE: cockroach sql --insecure
  - create database testmp_db;
  - set database = testmp_db;
  - CREATE TABLE "tbl_servers_visited"
        (
            "visit_id" SERIAL,
            "server_name" STRING(100),
            PRIMARY KEY ("visit_id")
        );
    - CREATE TABLE "tbl_server_details"
        (
            "server_name" STRING(100) NOT NULL,
            "ssl_grade_date" TIMESTAMPTZ,
            "previous_ssl_grade" STRING(10),
            "servers" JSONB NULL,
            PRIMARY KEY ("server_name")
        );
```

# El repo
El repo se encuentra en la siguiente ruta pública.
```sh
  - https://bitbucket.org/ingeniero_jean/testmp/src/master/
```

# Correr Backend
Desde el root del proyecto; sobre el mismo path del makefile.
```sh
  - make dev
```

# Correr Frontend
Ahora falta correr el front. Debes ir a 
```sh
  - cd .../testmp/src/static/front/
  - npm run serve
```

# Pruebas
Abre un navegador e ingresa el siguiente link
```sh
  - http://localhost:8082/
```
El puerto puede cambiar dependiendo de la disponibilidad.

### Tecnologías

Las tecnologias usadas fueron las establecidas en la prueba.

| Objetivo | Tipo |
| ------ | ------ |
| Base de Datos | CockroachDB |
| API Router | fasthttprouter |
| Frontend | Vue.js & bootstrap-vue.js |
| ORM | Ninguno |
| Backend | Golang |
| Versionamiento | Git - Bitbucket.org |

### Desarrollado por
Jean Carlos Rodríguez Medina - ingeniero.jean@gmail.com
